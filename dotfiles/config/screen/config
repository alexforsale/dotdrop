# default bindings:
#
# Note: all these commands started with the escape key,
# default to 'c-a' (ctrl-a)

# Automatically detach on hangup.
autodetach on   # default: on

# Don't display the copyright page
startup_message off # default: on

# Change default scrollback value for new windows
defscrollback 10000         # default: 100

# ESCAPE - the COMMAND CHARACTER
# set the escape key to z(I used ctrl-a alot so the default is very inconvenient.)
#escape ^Zz

# the following two lines give a two-line status, with the current window highlighted
hardstatus alwayslastline
hardstatus string '%{= kG}[ %{G}%H %{g}][%= %{= kw}%?%-Lw%?%{r}(%{W}%n*%f%t%?(%u)%?%{r})%{w}%?%+Lw%?%?%= %{g}][%{B} %m-%d %{W}%c %{g}]'']'

# 256 colors
attrcolor b ".I"
termcapinfo xterm 'Co#256:AB=\E[48;5;%dm:AF=\E[38;5;%dm'
defbce on

# Enable/disable multiuser mode. Standard screen operation is singleuser.
# In multiuser mode the commands acladd, aclchg, aclgrp and acldel can be used
# to enable (and disable) other user accessing this screen session.
# Requires suid-root.
multiuser off

# bufferfile:   The file to use for commands
#               "readbuf" ('<') and  "writebuf" ('>'):
bufferfile ${SCREENDIR}/.screen_exchange
#
# hardcopydir:  The directory which contains all hardcopies.
# hardcopydir           ~/.hardcopy
# hardcopydir           ~/.screen
#
# shell:  Default process started in screen's windows.
# Makes it possible to use a different shell inside screen
# than is set as the default login shell.
# If begins with a '-' character, the shell will be started as a login shell.
# shell                 zsh
# shell                 bash
# shell                 ksh
shell ${SHELL}

# emulate .logout message
pow_detach_msg "Screen session of \$LOGNAME \$:cr:\$:nl:ended."

# ignore displays that block on output
defnonblock on

# layouts
layout autosave on
layout new one
select 1
layout new two
select 1
split
resize -v +8
focus down
select 4
focus up
layout new three
select 1
split
resize -v +7
focus down
select 3
split -v
resize -h +10
focus right
select 4
focus up

layout attach one
layout select one

# switch windows with F3 (prev) and F4 (next)
bindkey "^[OR" prev
bindkey "^[OS" next

# switch layouts with Ctrl+F3 (prev layout) and Ctrl+F4 (next)
bindkey "^[O1;5R" layout prev
bindkey "^[O1;5S" layout next

# F2 puts Screen into resize mode. Resize regions using hjkl keys.
bindkey "^[OQ" eval "command -c rsz" # enter resize mode
# mouse tracking allows to switch region focus by clicking
mousetrack on

# navigating regions with Ctrl-arrows
bindkey "^[[1;5D" focus left
bindkey "^[[1;5C" focus right
bindkey "^[[1;5A" focus up
bindkey "^[[1;5B" focus down

# use hjkl keys to resize regions
bind -c rsz h eval "resize -h -5" "command -c rsz"
bind -c rsz j eval "resize -v -5" "command -c rsz"
bind -c rsz k eval "resize -v +5" "command -c rsz"
bind -c rsz l eval "resize -h +5" "command -c rsz"

# XTERM TWEAKS
# xterm understands both im/ic and doesn't have a status line.
# Note: Do not specify im and ic in the real termcap/info file as
# some programs (e.g. vi) will not work anymore.
termcap  xterm hs@:cs=\E[%i%d;%dr:im=\E[4h:ei=\E[4l
terminfo xterm hs@:cs=\E[%i%p1%d;%p2%dr:im=\E[4h:ei=\E[4

# tell screen that xterm can switch to dark background and has function
# keys.
termcapinfo xterm 'VR=\E[?5h:VN=\E[?5l'
termcapinfo xterm 'k1=\E[11~:k2=\E[12~:k3=\E[13~:k4=\E[14~'
termcapinfo xterm 'kh=\EOH:kI=\E[2~:kD=\E[3~:kH=\EOF:kP=\E[5~:kN=\E[6~'

# special xterm hardstatus: use the window title.
termcapinfo xterm 'hs:ts=\E]2;:fs=\007:ds=\E]2;screen\007'

# advertise hardstatus support to $TERMCAP
termcapinfo  * '' 'hs:ts=\E_:fs=\E\\:ds=\E_\E\\'
termcapinfo xterm* ti@:te@

altscreen on
